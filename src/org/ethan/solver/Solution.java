package org.ethan.solver;

import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.CarType;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.LastName;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

import java.util.Collection;

/**
 * Created by 40501 on 4/20/2015.
 */
public interface Solution {

    /*
        Methods return null if the rule cannot be applied. This should result in the rule passing
     */

    public Person getPerson(FirstName name);
    public Person getPerson(LastName name);
    public Person[] getPeople(int carNumber);
    public Person[] getPeople(CarType type);
    public Person[] getPeople(CarColor color);
    public Collection<Person> getAllPeople();
    public Car getCar(int number);
    public Car getCar(CarType type);
    public Car getCar(CarColor color);
    public Car getCar(FirstName name);
    public Car getCar(LastName name);
    public Car[] getAllCars();
}
