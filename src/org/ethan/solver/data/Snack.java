package org.ethan.solver.data;

/**
 * Created by 40501 on 4/20/2015.
 */
public enum Snack implements Data{
    //ALL(true, true, true, true), //NONE(false, false, false, false), EVERYONE HAD AT LEAST ONE SNACK. SEE RULE 11. THIS IMPROVES PERFORMANCE CONSIDERABLY

    /*NO_ICE_CREAM3(false, true, true, true),
        NO_COTTON_CANDY3(true, false, true, true), NO_POPCORN3(true, true, false, true),
        NO_SODA_3(true, true, true, false), */

    ICE_CREAM_AND_COTTON_CANDY(true, true, false, false), ICE_CREAM_AND_POPCORN(true, false, true, false),
        ICE_CREAM_AND_SODA(true, false, false, true), COTTON_CANDY_AND_POPCORN(false, true, true, false),
        COTTON_CANDY_AND_SODA(false, true, false, true), POPCORN_AND_SODA(false, false, true, true),

    ICE_CREAM(true, false, false, false), COTTON_CANDY(false, true, false, false), POPCORN(false, false, true, false),
            SODA(false, false, false, true);


    private boolean iceCream;
    private boolean cottonCandy;
    private boolean popcorn;
    private boolean soda;

    private int numberOfItems;

    Snack(boolean iceCream, boolean cottonCandy, boolean popcorn, boolean soda) {
        this.iceCream = iceCream;
        this.cottonCandy = cottonCandy;
        this.popcorn = popcorn;
        this.soda = soda;

        numberOfItems = 0;
        if (iceCream) {
            numberOfItems++;
        }
        if(cottonCandy) {
            numberOfItems++;
        }
        if(popcorn) {
            numberOfItems++;
        }
        if (soda) {
            numberOfItems++;
        }
    }



    public boolean isIceCream() {
        return iceCream;
    }

    public boolean isCottonCandy() {
        return cottonCandy;
    }

    public boolean isPopcorn() {
        return popcorn;
    }

    public boolean isSoda() {
        return soda;
    }

    @Override
    public Type getType() {
        return Type.SNACK;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public static boolean isSame(Snack s1, Snack s2) {
        boolean[] b1 = s1.asArray();
        boolean[] b2 = s2.asArray();
        for (int i = 0; i < b1.length; i++) {
            if (b1[i] && b2[i]) {
                return true;
            }
        }
        return false;
    }

    public boolean[] asArray() {
        return new boolean[] {iceCream, cottonCandy, popcorn, soda};
    }
}
