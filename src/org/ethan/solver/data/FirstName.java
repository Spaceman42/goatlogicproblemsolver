package org.ethan.solver.data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum FirstName implements Data {
    BILL, RAYMOND, THERESA, TRAVIS, MONIQUE, PENNY, PIERRE, GLORIA, CINDY, TRACI, CHARLES, GREGORY;

    @Override
    public Type getType() {
        return Type.FIRST_NAME;
    }


    public char getInitial() {
        return toString().charAt(0);
    }
}
