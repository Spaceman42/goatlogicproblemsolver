package org.ethan.solver.data;

/**
 * Created by 40501 on 4/20/2015.
 */
public interface Data {
    public enum Type {
        COLOR, TYPE, FIRST_NAME, LAST_NAME, SNACK, GENDER;
    }


    public Type getType();
}
