package org.ethan.solver.data;

/**
 * Created by Ethan on 4/20/2015.
 */
public enum Gender implements Data{
    MALE, FEMALE;


    public Type getType() {
        return Type.GENDER;
    }
}
