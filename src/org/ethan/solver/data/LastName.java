package org.ethan.solver.data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum LastName implements Data{
    ALTMAN, ANDERSON, CHOU, COLLINS, DANZA, LANDIS, SANCHEZ, TOWNSEND, VANDERGRIFT, ZEIGLER, ZETESLO, MARTINI;

    @Override
    public Type getType() {
        return Type.LAST_NAME;
    }

    public char getInitial() {
        return toString().charAt(0);
    }
}
