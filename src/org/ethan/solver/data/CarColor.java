package org.ethan.solver.data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum CarColor implements Data{
    RED, ORANGE, BLUE, YELLOW, PURPLE, BLACK, PINK, SILVER, GREEN, WHITE;


    @Override
    public Type getType() {
        return Type.COLOR;
    }
}
