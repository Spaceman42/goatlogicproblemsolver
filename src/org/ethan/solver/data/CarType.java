package org.ethan.solver.data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum CarType implements Data{
    GOAT(true), DRAGON(false), LION(true), CAT(true), DOG(true), ROOSTER(false), EAGLE(false), FOX(true), HORSE(true), PIG(true);


    public final boolean mammal;

    CarType(boolean mammal) {
        this.mammal = mammal;
    }

    @Override
    public Data.Type getType() {
        return Data.Type.TYPE;
    }
}
