package org.ethan.solver.rule;

/**
 * Created by Ethan on 4/20/2015.
 */
public enum Evaluation {
    PASS, FAIL, NOT_APPLICABLE;



    public static Evaluation failOnAnyFail(Evaluation[] results) {
        int na = 0;
        for (int i = 0; i < results.length; i++) {
            if (results[i] == FAIL) {
                return FAIL;
            } else if (results[i] == NOT_APPLICABLE) {
                na++;
            }
        }
        if (na == results.length) {
            return NOT_APPLICABLE;
        }
        return PASS;
    }
}
