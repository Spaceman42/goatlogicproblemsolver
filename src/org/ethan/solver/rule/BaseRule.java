package org.ethan.solver.rule;

import org.ethan.solver.Solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ethan on 4/20/2015.
 */
public abstract class BaseRule implements Rule{


    List<Rule> rules;


    public BaseRule() {
        rules = new ArrayList<Rule>();
    }


    public void addRule(Rule r) {
        rules.add(r);
    }

    @Override
    public Evaluation evaluateSolution(Solution s) {
        for(Rule r : rules) {
            Evaluation result = r.evaluateSolution(s);
            if (result == Evaluation.FAIL) {
                return Evaluation.FAIL;
            }
            if (result == Evaluation.NOT_APPLICABLE) {
                Evaluation handle = handleNotApplicable(r, s);

                if (handle == Evaluation.FAIL) {
                    return Evaluation.FAIL;
                }
            }
        }

        return Evaluation.PASS;
    }

    protected abstract Evaluation handleNotApplicable(Rule rule, Solution s);
}
