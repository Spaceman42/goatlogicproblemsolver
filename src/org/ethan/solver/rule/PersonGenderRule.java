package org.ethan.solver.rule;

import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.Gender;
import org.ethan.solver.data.LastName;
import org.ethan.solver.solution.Person;

/**
 * Created by 40501 on 4/21/2015.
 */
public class PersonGenderRule {


    public boolean evaluate(Person p) {
        return firstNameConsistent(p.getFirstName(), p.getGender()) && lastNameConsistent(p.getLastName(), p.getGender());
    }

    private boolean firstNameConsistent(FirstName name, Gender gender) {
        if (gender == Gender.MALE) {
            if (name == FirstName.THERESA) {
                return false;
            }
            if (name == FirstName.TRACI) {
                return false;
            }
            if (name == FirstName.GLORIA) {
                return false;
            }
            if (name == FirstName.MONIQUE) {
                return false;
            }
            if (name == FirstName.CINDY) {
                return false;
            }
            if (name == FirstName.PENNY) {
                return false;
            }
        } else {
            if (name == FirstName.BILL) {
                return false;
            }
            if (name == FirstName.GREGORY) {
                return false;
            }
            if (name == FirstName.RAYMOND) {
                return false;
            }
            if (name == FirstName.PIERRE) {
                return false;
            }
            if (name == FirstName.TRAVIS) {
                return false;
            }
            if (name == FirstName.CHARLES) {
                return false;
            }
        }
        return true;
    }

    private boolean lastNameConsistent(LastName name, Gender gender) {
        if (gender == Gender.MALE) {
            if (name == LastName.CHOU) {
                return false;
            }
            if (name == LastName.SANCHEZ) {
                return false;
            }
            if (name == LastName.ZEIGLER) {
                return false;
            }
            if (name == LastName.TOWNSEND) {
                return false;
            }
            if (name == LastName.VANDERGRIFT) {
                return false;
            }
        } else {
            if (name == LastName.ANDERSON) {
                return false;
            }
            if (name == LastName.LANDIS) {
                return false;
            }
        }

        return true;
    }
}
