package org.ethan.solver.rule.ruleset.rule2;

import org.ethan.solver.Solution;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Person;

import java.util.Collection;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule2 implements Rule {

    /*
    3 of the 4 snacks available were selected by the same # of people.
    The other snack was chosen by 5 different people. Only 1 pair of riders ate the same snack.
     */

    @Override
    public Evaluation evaluateSolution(Solution s) {
        Collection<Person> people = s.getAllPeople();

        if (people == null || people.size() != GivenRules.NUM_PEOPLE) {
            return Evaluation.NOT_APPLICABLE;
        }
        return Evaluation.PASS;
        //TODO: implement
    }
}
