package org.ethan.solver.rule.ruleset.rule18;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.CarType;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.LastName;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule18 implements Rule {
    /*
    Pierre (who did not ride with a girl), rode directly in front of Vandergrift.
    Gloria (who is not Ms. Townsend) was immediately behind Zeteslo.
    Martini rode in the car(which wasn't fox) which was direcly in front of the purple car.
    These 3 people: Gregory, Landis, and Traci were in consecutive order.
     */
    @Override
    public Evaluation evaluateSolution(Solution s) {
        Evaluation[] results = new Evaluation[4];
        results[0] = pierreTest(s);
        results[1] = gloriaTest(s);
        results[2] = martiniTest(s);
        results[3] = orderTest(s);
        return Evaluation.failOnAnyFail(results);
    }

    private Evaluation pierreTest(Solution s) {
        Person pierre = s.getPerson(FirstName.PIERRE);
        if (pierre == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (pierre.getCar().getNumber() == GivenRules.NUM_CARTS-1) { //Can't be last
            return Evaluation.FAIL;
        }
        Person vandergrift = s.getPerson(LastName.VANDERGRIFT);
        if (vandergrift != null) {
            if (pierre.getCar().getNumber() != vandergrift.getCar().getNumber() - 1) {
                return Evaluation.FAIL;
            }
        }
        return Evaluation.PASS;
    }
    //Gloria (who is not Ms. Townsend) was immediately behind Zeteslo.
    private Evaluation gloriaTest(Solution s) {
        Person gloria = s.getPerson(FirstName.GLORIA);
        if (gloria == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (gloria.getCar().getNumber() == 0) { //Can't be first
            return Evaluation.FAIL;
        }
        if (gloria.getLastName() == LastName.TOWNSEND) {
            return Evaluation.FAIL;
        }
        Person zeteslo = s.getPerson(LastName.ZETESLO);
        if (zeteslo != null) {
            if (gloria.getCar().getNumber() != zeteslo.getCar().getNumber() + 1) {
                return Evaluation.FAIL;
            }
        }
        return Evaluation.PASS;
    }
    //Martini rode in the car(which wasn't fox) which was direcly in front of the purple car.
    private Evaluation martiniTest(Solution s) {
        Person martini = s.getPerson(LastName.MARTINI);
        if (martini == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (martini.getCar().getType() == CarType.FOX) {
            return Evaluation.FAIL;
        }
        if (martini.getCar().getNumber() == GivenRules.NUM_CARTS-1) { //can't be last
            return Evaluation.FAIL;
        }
        if (s.getCar(martini.getCar().getNumber() + 1) != null) {
            if (s.getCar(martini.getCar().getNumber() + 1).getColor() != CarColor.PURPLE) {
                return Evaluation.FAIL;
            }
        }
        return Evaluation.PASS;
    }

    //These 3 people: Gregory, Landis, and Traci were in consecutive order.
    //I assume the order is greg, landis, traci which may not be true. TODO: fix
    private Evaluation orderTest(Solution s) {
        Person greg = s.getPerson(FirstName.GREGORY);
        Person landis = s.getPerson(LastName.LANDIS);
        Person traci = s.getPerson(FirstName.TRACI);
        if (greg == null || landis == null || traci == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        int gregNum = greg.getCar().getNumber();
        int landisNum = landis.getCar().getNumber();
        int traciNum = landis.getCar().getNumber();

        if (gregNum != landisNum - 1 && landisNum != traciNum - 1) { //Can rewrite to be used in single check greg can't be last etc..
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }
}