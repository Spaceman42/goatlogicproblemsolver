package org.ethan.solver.rule.ruleset.rule12;

import org.ethan.solver.Solution;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule12 implements Rule {
    //Altogether, there were an equal number of people in the odd number and the even number cars.
    @Override
    public Evaluation evaluateSolution(Solution s) {
        Car[] cars = s.getAllCars();
        if (cars == null || cars.length != GivenRules.NUM_CARTS) {
            return Evaluation.NOT_APPLICABLE;
        }

        int odds = 0;
        for (int i = 0; i < cars.length; i += 2) {
            odds += cars[i].getNumPeople();
        }
        int evens = 0;
        for (int i = 1; i < cars.length; i += 2) {
            evens += cars[i].getNumPeople();
        }

        if (evens != odds) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

}
