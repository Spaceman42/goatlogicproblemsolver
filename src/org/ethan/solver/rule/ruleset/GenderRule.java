package org.ethan.solver.rule.ruleset;

import org.ethan.solver.Solution;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.Gender;
import org.ethan.solver.data.LastName;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.solution.Person;

import java.util.Collection;

/**
 * Created by Ethan on 4/20/2015.
 */
public class GenderRule implements Rule{


    @Override
    public Evaluation evaluateSolution(Solution s) {
        Collection<Person> people = s.getAllPeople();
        if (people == null) {
            return Evaluation.NOT_APPLICABLE;
        }

        for (Person p : people) {
            if (p != null) {
                Gender g = p.getGender();

                if (!firstNameConsistent(p.getFirstName(), g)) {
                    return Evaluation.FAIL;
                }
                if (!lastNameConsistent(p.getLastName(), g)) {
                    return Evaluation.FAIL;
                }
            }
        }

        return Evaluation.PASS;
    }


    private boolean firstNameConsistent(FirstName name, Gender gender) {
        if (gender == Gender.MALE) {
            if (name == FirstName.THERESA) {
                return false;
            }
            if (name == FirstName.TRACI) {
                return false;
            }
            if (name == FirstName.GLORIA) {
                return false;
            }
            if (name == FirstName.MONIQUE) {
                return false;
            }
            if (name == FirstName.CINDY) {
                return false;
            }
            if (name == FirstName.PENNY) {
                return false;
            }
        } else {
            if (name == FirstName.BILL) {
                return false;
            }
            if (name == FirstName.GREGORY) {
                return false;
            }
            if (name == FirstName.RAYMOND) {
                return false;
            }
            if (name == FirstName.PIERRE) {
                return false;
            }
            if (name == FirstName.TRAVIS) {
                return false;
            }
            if (name == FirstName.CHARLES) {
                return false;
            }
        }
        return true;
    }

    private boolean lastNameConsistent(LastName name, Gender gender) {
        if (gender == Gender.MALE) {
            if (name == LastName.CHOU) {
                return false;
            }
            if (name == LastName.SANCHEZ) {
                return false;
            }
            if (name == LastName.ZEIGLER) {
                return false;
            }
            if (name == LastName.TOWNSEND) {
                return false;
            }
            if (name == LastName.VANDERGRIFT) {
                return false;
            }
        } else {
            if (name == LastName.ANDERSON) {
                return false;
            }
            if (name == LastName.LANDIS) {
                return false;
            }
        }

        return true;
    }
}
