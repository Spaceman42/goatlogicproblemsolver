package org.ethan.solver.rule.ruleset.rule20;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarType;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.LastName;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.solution.Car;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule20 implements Rule {
    //Ms. Vandergrift thought that the lion was too scary to ride on. Cindy felt the same way about the dragon.

    @Override
    public Evaluation evaluateSolution(Solution s) {
        for (Car c : s.getAllCars()) {
            if (c.getType() == CarType.LION && c.containsValue(LastName.VANDERGRIFT)) {
                return Evaluation.FAIL;
            } else if (c.getType() == CarType.DRAGON && c.containsValue(FirstName.CINDY)) {
                return Evaluation.FAIL;
            }
        }
        return Evaluation.PASS;
    }
}
