package org.ethan.solver.rule.ruleset.rule3;

import org.ethan.solver.Solution;
import org.ethan.solver.data.LastName;
import org.ethan.solver.data.Snack;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule3 implements Rule {
    /*
    Danza had only ice-cream. Zeteslo did not have popcorn or cotton candy.
    Townsend did not drink soda.
    */
    @Override
    public Evaluation evaluateSolution(Solution s) {
        Evaluation danza = checkDanza(s);
        Evaluation zeteslo = checkZeteslo(s);
        Evaluation townsend = checkTownsend(s);

        if (danza == Evaluation.NOT_APPLICABLE && zeteslo == Evaluation.NOT_APPLICABLE && townsend == Evaluation.NOT_APPLICABLE) {
            return Evaluation.NOT_APPLICABLE;
        }
        //If any fail, the rule fails
        if (danza == Evaluation.FAIL || zeteslo == Evaluation.FAIL || townsend == Evaluation.FAIL) {
            return Evaluation.FAIL;
        }
        //otherwise pass
        return Evaluation.PASS;
    }

    //Danza had only ice-cream.
    private Evaluation checkDanza(Solution s) {
        Person danza = s.getPerson(LastName.DANZA);
        if (danza == null) {
            return Evaluation.NOT_APPLICABLE;
        }

        if (danza.getSnack() != Snack.ICE_CREAM) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }
    //Zeteslo did not have popcorn or cotton candy.
    private Evaluation checkZeteslo(Solution s) {
        Person zeteslo = s.getPerson(LastName.ZETESLO);
        if (zeteslo == null) {
            return Evaluation.NOT_APPLICABLE;
        }

        if (zeteslo.getSnack().isPopcorn() || zeteslo.getSnack().isCottonCandy()) {
            return Evaluation.FAIL;
        }

        return Evaluation.PASS;
    }
    //Townsend did not drink soda
    private Evaluation checkTownsend(Solution s) {
        Person townsend = s.getPerson(LastName.TOWNSEND);
        if (townsend == null) {
            return Evaluation.NOT_APPLICABLE;
        }

        if (townsend.getSnack().isSoda()) {
            return Evaluation.FAIL;
        }

        return Evaluation.PASS;
    }
}
