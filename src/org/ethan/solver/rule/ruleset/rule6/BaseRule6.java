package org.ethan.solver.rule.ruleset.rule6;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.CarType;
import org.ethan.solver.data.LastName;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule6 implements Rule {
    /*
    Ms. Chou (who ate ice-cream) rode in car #3 and not in yellow, purple, or black cars.
    None of the group rode in the white car or the rooster car (which was farther back than the pink car)
     */

    @Override
    public Evaluation evaluateSolution(Solution s) {
        Evaluation[] results = new Evaluation[4];
        results[0] = chouCheck(s);
        results[1] = whiteCheck(s);
        results[2] = roosterCheck(s);
        results[3] = distanceCheck(s);
        int numNA = 0;
        for (Evaluation e : results) {
            if (e == Evaluation.NOT_APPLICABLE) {
                numNA++;
            } else if (e == Evaluation.FAIL) {
                return Evaluation.FAIL;
            }
        }
        return Evaluation.PASS;
    }

    private Evaluation distanceCheck(Solution s) { // rooster further back than pink
        Car roosterCar = s.getCar(CarType.ROOSTER);
        if (roosterCar == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        Car pinkCar = s.getCar(CarColor.PINK);
        if (pinkCar == null) {
            return Evaluation.NOT_APPLICABLE;
        }

        if (roosterCar.getNumber() > pinkCar.getNumber()) {
            return Evaluation.PASS;
        }
        return Evaluation.FAIL;
    }
    //None of the group rode in the white car or the rooster car
    private Evaluation roosterCheck(Solution s) {
       Car c = s.getCar(CarType.ROOSTER);
       if (c == null) {
           return Evaluation.NOT_APPLICABLE;
       }
       if (c.getNumber() != 0) {
           return Evaluation.FAIL;
       }
        return Evaluation.PASS;
    }

    private Evaluation whiteCheck(Solution s) {
        Car c = s.getCar(CarColor.WHITE);
        if (c == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (c.getNumber() != 0) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

    //Ms. Chou (who ate ice-cream) rode in car #3 and not in yellow, purple, or black cars.
    private Evaluation chouCheck(Solution s) {
        Person chou = s.getPerson(LastName.CHOU);
        if (chou == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (!chou.getSnack().isIceCream()) {
            return Evaluation.FAIL;
        }
        Car c = chou.getCar();
        if (c.getNumber() != 3) {
            return Evaluation.FAIL;
        }
        CarColor color = c.getColor();
        if (color == CarColor.YELLOW || color == CarColor.PURPLE || color == CarColor.BLACK) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }
}
