package org.ethan.solver.rule.ruleset.rule11;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.Gender;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Person;

import java.util.Collection;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule11 implements Rule {
    //Although everyone selected at least one snack, only two boys picked more than one item. No one in the silver, 8th or 9th cars had soda.
    @Override
    public Evaluation evaluateSolution(Solution s) {
        //Everyone having at least one snack is handled in the Snack enum. There is no chance of a person being generated with 0 snacks

        Collection<Person> people = s.getAllPeople(); //can't be null;

        int boysWithSnacks = 0; //Only 2 boys can have numberOfItems > 1
        for (Person p : people) {
            if (p != null ) {
                if (p.getGender() == Gender.MALE) {
                    if (p.getSnack().getNumberOfItems() > 1) {
                        boysWithSnacks++;
                    }
                }

                if (p.getCar().getColor() == CarColor.SILVER || p.getCar().getNumber() == 8 || p.getCar().getNumber() == 9) {
                    if (p.getSnack().isSoda()) {
                        return Evaluation.FAIL;
                    }
                }
            }
        }
        if (s.getAllCars().length == GivenRules.NUM_CARTS && boysWithSnacks != 2) {
            return Evaluation.FAIL;
        }

        return Evaluation.PASS;
    }

}
