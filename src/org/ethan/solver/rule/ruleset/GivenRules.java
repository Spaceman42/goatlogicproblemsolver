package org.ethan.solver.rule.ruleset;

import org.ethan.solver.Solution;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.rule1.BaseRule1;
import org.ethan.solver.rule.ruleset.rule10.BaseRule10;
import org.ethan.solver.rule.ruleset.rule11.BaseRule11;
import org.ethan.solver.rule.ruleset.rule12.BaseRule12;
import org.ethan.solver.rule.ruleset.rule13.BaseRule13;
import org.ethan.solver.rule.ruleset.rule14.BaseRule14;
import org.ethan.solver.rule.ruleset.rule15.BaseRule15;
import org.ethan.solver.rule.ruleset.rule16.BaseRule16;
import org.ethan.solver.rule.ruleset.rule17.BaseRule17;
import org.ethan.solver.rule.ruleset.rule18.BaseRule18;
import org.ethan.solver.rule.ruleset.rule19.BaseRule19;
import org.ethan.solver.rule.ruleset.rule2.BaseRule2;
import org.ethan.solver.rule.ruleset.rule20.BaseRule20;
import org.ethan.solver.rule.ruleset.rule21.BaseRule21;
import org.ethan.solver.rule.ruleset.rule22.BaseRule22;
import org.ethan.solver.rule.ruleset.rule23.BaseRule23;
import org.ethan.solver.rule.ruleset.rule24.BaseRule24;
import org.ethan.solver.rule.ruleset.rule25.BaseRule25;
import org.ethan.solver.rule.ruleset.rule3.BaseRule3;
import org.ethan.solver.rule.ruleset.rule4.BaseRule4;
import org.ethan.solver.rule.ruleset.rule5.BaseRule5;
import org.ethan.solver.rule.ruleset.rule6.BaseRule6;
import org.ethan.solver.rule.ruleset.rule7.BaseRule7;
import org.ethan.solver.rule.ruleset.rule8.BaseRule8;
import org.ethan.solver.rule.ruleset.rule9.BaseRule9;

/**
 * Created by Ethan on 4/20/2015.
 */
public class GivenRules extends BaseRule {
    public static final int NUM_PEOPLE = 12;
    public static final int NUM_CARTS = 10;

    public GivenRules() {
        addRule(new BaseRule1());
        addRule(new BaseRule2());
        addRule(new BaseRule3());
        addRule(new BaseRule4());
        addRule(new BaseRule5());
        addRule(new BaseRule6());
        addRule(new BaseRule7());
        addRule(new BaseRule8());
        addRule(new BaseRule9());
        addRule(new BaseRule10());
        addRule(new BaseRule11());
        addRule(new BaseRule12());
        addRule(new BaseRule13());
        addRule(new BaseRule14());
        addRule(new BaseRule15());
        addRule(new BaseRule16());
        addRule(new BaseRule17());
        addRule(new BaseRule18());
        addRule(new BaseRule19());
        addRule(new BaseRule20());
        addRule(new BaseRule21());
        addRule(new BaseRule22());
        addRule(new BaseRule23());
        addRule(new BaseRule24());
        addRule(new BaseRule25());
    }


    @Override
    protected Evaluation handleNotApplicable(Rule rule, Solution s) {
        return null;
    }
}
