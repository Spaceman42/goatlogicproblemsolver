package org.ethan.solver.rule.ruleset.rule4;

import org.ethan.solver.Solution;
import org.ethan.solver.data.Gender;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule4 implements Rule {
    //Although greater # of even numbered cars were occupied,
    // twice as many girls rode in odd numbered cars than in even numbered cars.
    @Override
    public Evaluation evaluateSolution(Solution s) {
        Car[] cars = s.getAllCars();
        if (cars == null || cars.length != GivenRules.NUM_CARTS) {
            return Evaluation.NOT_APPLICABLE;
        }
        //Do odd computations
        int numOddPeople = 0;
        int oddGirls = 0;
        for (int i = 0; i < cars.length; i+=2) { //ALL ODD CARS
            int numCar = cars[i].getNumPeople();
            numOddPeople+=numCar;
            if (numCar > 0) {
                for (Person p : cars[i].getPeople()) {
                    if (p != null) {
                        if (p.getGender() == Gender.FEMALE) {
                            oddGirls ++;
                        }
                    }
                }
            }
        }
        //Early out to avoid even computations
        if (numOddPeople > GivenRules.NUM_PEOPLE/2) {
            //If the number of people in odd cars is equal to or greater than half of tht total people this can't be true
            return Evaluation.FAIL;
        }
        int evenGirls = 0;
        for (int i = 1; i < cars.length; i+=2) { //ALL EVEN CARS
            for (Person p : cars[i].getPeople()) {
                if (p != null && p.getGender() == Gender.FEMALE) {
                    evenGirls ++;
                }
            }
        }

        //Even computation out
        // twice as many girls rode in odd numbered cars than in even numbered cars.
        if (oddGirls != evenGirls*2) {
            return Evaluation.FAIL;
        }

        return Evaluation.PASS;
    }
}
