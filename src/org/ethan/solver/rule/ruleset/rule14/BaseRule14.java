package org.ethan.solver.rule.ruleset.rule14;

import org.ethan.solver.Solution;
import org.ethan.solver.data.Gender;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Person;

import java.util.Collection;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule14 implements Rule {
    //Only one girl had popcorn. No girl chose more than 1 snack item.

    @Override
    public Evaluation evaluateSolution(Solution s) {
        Collection<Person> people = s.getAllPeople();
        int popcorns = 0;
        for (Person p : people) {
            if (p != null && p.getGender() == Gender.FEMALE) {
                if (p.getSnack().getNumberOfItems() > 1) {
                    return Evaluation.FAIL;
                }
                if (p.getSnack().isPopcorn()) {
                    popcorns++;
                }
            }
        }
        if (popcorns > 1) { //Might help in the CarSolver
            return Evaluation.FAIL;
        }
        if (s.getAllCars().length == GivenRules.NUM_CARTS && popcorns != 1) {
            return Evaluation.FAIL;
        }

        return Evaluation.PASS;
    }
}
