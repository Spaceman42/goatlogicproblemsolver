package org.ethan.solver.rule.ruleset.rule19;

import org.ethan.solver.Solution;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.Gender;
import org.ethan.solver.data.LastName;
import org.ethan.solver.data.Snack;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule19 implements Rule {

    /*
    Cindy and Ms. Sanchez had the same snack, and they were the only 2 girls who had it.
    Although Penny and Theresa did not have the same snack, neither of them had cotton candy or popcorn.
     */
    @Override
    public Evaluation evaluateSolution(Solution s) {
        return Evaluation.failOnAnyFail(new Evaluation[] {pennyCheck(s), cindyCheck(s)});
    }

    private Evaluation pennyCheck(Solution s) {
        Person penny = s.getPerson(FirstName.PENNY);
        Person theresa = s.getPerson(FirstName.THERESA);
        if (penny == null && theresa == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (penny != null) {
            if (penny.getSnack().isCottonCandy() || penny.getSnack().isPopcorn()) {
                return Evaluation.FAIL;
            }
        }
        if (theresa != null) {
            if (theresa.getSnack().isCottonCandy() || theresa.getSnack().isPopcorn()) {
                return Evaluation.FAIL;
            }
        }
        if (theresa != null && penny != null) {
            if (Snack.isSame(theresa.getSnack(), penny.getSnack())) {
                return Evaluation.FAIL;
            }
        }
        return Evaluation.PASS;
    }

    //Cindy and Ms. Sanchez had the same snack, and they were the only 2 girls who had it.
    private Evaluation cindyCheck(Solution s) {
        if (s.getAllCars().length != GivenRules.NUM_CARTS) {
            return Evaluation.NOT_APPLICABLE;
        }
        Snack cindy = s.getPerson(FirstName.CINDY).getSnack();
        Snack sanchez = s.getPerson(LastName.SANCHEZ).getSnack();

        if (!Snack.isSame(cindy, sanchez)) {
            return Evaluation.FAIL;
        }
        int numSame = 0;
        for (Person p : s.getAllPeople()) {
            if (p != null) {
                if (p.getGender() == Gender.FEMALE) {
                    if (Snack.isSame(p.getSnack(), cindy)) {
                        numSame++;
                    }
                }
            }
        }
        if (numSame != 2) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }
}
