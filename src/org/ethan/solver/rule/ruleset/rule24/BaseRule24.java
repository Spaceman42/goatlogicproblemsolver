package org.ethan.solver.rule.ruleset.rule24;

import org.ethan.solver.Solution;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule24 extends BaseRule {
    @Override
    protected Evaluation handleNotApplicable(Rule rule, Solution s) {
        return Evaluation.PASS;
    }
}
