package org.ethan.solver.rule.ruleset.rule7;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarType;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule7 implements Rule {
    //Bill did not ride in the dog car, nor did he ride with Theresa.
    @Override
    public Evaluation evaluateSolution(Solution s) {
        Person bill = s.getPerson(FirstName.BILL);
        if (bill == null) {
            return Evaluation.NOT_APPLICABLE;
        }

        if (bill.getCar().getType() == CarType.DOG) {
            return Evaluation.FAIL;
        }

        if (bill.getCar().getNumPeople() == 2) {
            for (Person p : bill.getCar().getPeople()) {
                if (p.getFirstName() == FirstName.THERESA) {
                    return Evaluation.FAIL;
                }
            }
        }
        return Evaluation.PASS;
    }
}