package org.ethan.solver.rule.ruleset.rule17;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.Gender;
import org.ethan.solver.data.LastName;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule17 implements Rule {
    /*
    Penny and Ms. Zeigler had ice cream. One of the boys did too. Only 1 boy had cotton candy (not Pierre or Bill).
    Of the ones who rode in the purple car, one had popcorn and one had cotton candy (and they had no other snacks).
     */

    @Override
    public Evaluation evaluateSolution(Solution s) {
        Evaluation[] results = new Evaluation[4];
        results[0] = hasIceCream(s.getPerson(FirstName.PENNY));
        results[1] = hasIceCream(s.getPerson(LastName.ZEIGLER));
        results[2] = boyCheck(s);
        results[3] = purpleCheck(s);

        return Evaluation.failOnAnyFail(results);
    }

    private Evaluation purpleCheck(Solution s) {
        Car c = s.getCar(CarColor.PURPLE);
        if (c == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        int popcorn = 0;
        int cc = 0;
        for (Person p : c.getPeople()) {
            if (p != null) {
                if (p.getSnack().getNumberOfItems() != 1) {
                    return Evaluation.FAIL;
                }
                if (p.getSnack().isPopcorn()) {
                    popcorn++;
                }
                if (p.getSnack().isCottonCandy()) {
                    cc++;
                }
            }
        }
        if (popcorn != 1 || cc != 1) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

    private Evaluation hasIceCream(Person p) {
        if (p == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (!p.getSnack().isIceCream()) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

    private Evaluation boyCheck(Solution s) {
        int ccNum = -1;
        int icNum = -1;
        for (Person p : s.getAllPeople()) {
            if (p != null && p.getGender() == Gender.MALE) {
                if (p.getSnack().isIceCream()) {
                    icNum ++;
                }
                if (p.getSnack().isCottonCandy()) {
                    ccNum++;
                    if (p.getFirstName() == FirstName.PIERRE || p.getFirstName() == FirstName.BILL) {
                        return Evaluation.FAIL;
                    }
                }
            }
        }
        if (s.getAllCars().length == GivenRules.NUM_CARTS) {
            if (ccNum != 1 || icNum != 1) {
                return Evaluation.FAIL;
            }
        }
        return Evaluation.PASS;
    }
}
