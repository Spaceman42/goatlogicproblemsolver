package org.ethan.solver.rule.ruleset.rule9;

import org.ethan.solver.Solution;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule9 implements Rule {
    //Nobody in the first 3 cars had soda and at least one person in the 7th car did. No one in the last five cars had ice cream.


    @Override
    public Evaluation evaluateSolution(Solution s) {
        Car[] cars = s.getAllCars();

        for (Car c : cars) {
            if (c.getNumber() < 3) {
                for (Person p : c.getPeople()) {
                    if (p != null && p.getSnack().isSoda()) {
                        return Evaluation.FAIL; // No one in the first 3 cars had soda
                    }
                }
            } else if (c.getNumber() == 7) { //Someone in car 7 has soda
                int numSoda = 0;
                for (Person p : c.getPeople()) {
                    if (p != null && p.getSnack().isSoda()) {
                        numSoda ++;
                    }
                }
                if (numSoda == 0) {
                    return Evaluation.FAIL;
                }
            }
            if (c.getNumber() > (GivenRules.NUM_CARTS/2) - 1) { //No one in last 5 cars....
                for (Person p : c.getPeople()) {
                    if (p != null && p.getSnack().isIceCream()) {
                        return Evaluation.FAIL;
                    }
                }
            }
        }

        return Evaluation.PASS;
    }
}
