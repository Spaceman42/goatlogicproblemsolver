package org.ethan.solver.rule.ruleset.rule21;

import org.ethan.solver.Solution;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule21 implements Rule {
    // These 3 people were in consecutive order:
    // Danza (who did not ride next to the yellow car),
    // Altman (who did not ride in the blue car), and
    // Pierre (who wasn’t in yellow or blue cars).
    @Override
    public Evaluation evaluateSolution(Solution s) {
        return null;
    }
}
