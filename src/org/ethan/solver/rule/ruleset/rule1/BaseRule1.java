package org.ethan.solver.rule.ruleset.rule1;

import org.ethan.solver.Solution;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

import java.util.Collection;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule1 implements Rule {
    /*
     At least 3 of the cars contained male/female pairs. Only 2 of the 12 friends rode alone. The other 10 rode in pairs.
     */


    @Override
    public Evaluation evaluateSolution(Solution s) {
        Collection<Person> people = s.getAllPeople();
        if (people == null || (people.size() != GivenRules.NUM_PEOPLE)) {
            return Evaluation.NOT_APPLICABLE;
        }

        int numPairs = 0;
        int numGenderPairs = 0;
        int numAlone = 0;

        for (Person p : people) {
            if (p != null) {
                if (p.getCar().getPeople()[0] != null && p.getCar().getPeople()[1] != null) {
                    numPairs++;
                    if (isGenderPair(p.getCar())) {
                        numGenderPairs++;
                    }
                } else {
                    numAlone++;
                }
            }
        }
        if (numAlone != 2) {
            return Evaluation.FAIL;
        }
        if (numGenderPairs < 3) {
            return Evaluation.FAIL;
        }
        if (numPairs != 10) {
            return Evaluation.FAIL;
        }

        return Evaluation.PASS;
    }


    private boolean isGenderPair(Car c) {
        return c.getPeople()[0].getGender() != c.getPeople()[1].getGender();
    }
}
