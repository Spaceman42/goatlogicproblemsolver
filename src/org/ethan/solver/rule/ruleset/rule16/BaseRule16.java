package org.ethan.solver.rule.ruleset.rule16;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarType;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule16 implements Rule {
    /*
     The names of the people who rode in the horse car did not have any initials in common (with each other or themselves).
            This is also true of the riders in the lion car. In fact, this is the case in all but 2 of the cars.
     */
    //TODO: all but 2 cars

    @Override
    public Evaluation evaluateSolution(Solution s) {
        int numViolations = -1;
        for (Car c : s.getAllCars()) {
            if (c.getType() == CarType.HORSE || c.getType() == CarType.LION) {
                if (sameInitials(c.getPerson(0), c.getPerson(1))) {
                    return Evaluation.FAIL;
                }
            }
            if (s.getAllCars().length == GivenRules.NUM_CARTS) {
                if (sameInitials(c.getPerson(0), c.getPerson(1))) {
                    numViolations++;
                }
            }
        }

        if (numViolations != -1 && numViolations != 2) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }


    private boolean sameInitials(Person p1, Person p2) {
        if (p1 != null) {
            if (p2 != null) {
                return sameInitialsFull(p1, p2);
            }
            return sameInitalHalf(p1);
        }
        if (p2 != null) {
            if (p1 != null) {
                return sameInitialsFull(p1, p2);
            }
            return sameInitalHalf(p2);
        }
        return false;
    }

    private boolean sameInitialsFull(Person p1, Person p2) {
        char[] initials = new char[4];
        initials[0] = p1.getFirstName().getInitial();
        initials[1] = p1.getLastName().getInitial();
        initials[2] = p2.getFirstName().getInitial();
        initials[3] = p2.getLastName().getInitial();
        for (char c1 : initials) {
            for (char c2 : initials) {
                if (c1 == c2) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean sameInitalHalf(Person p) {
        char[] initials = new char[2];
        initials[0] = p.getFirstName().getInitial();
        initials[1] = p.getLastName().getInitial();
        if (initials[0] == initials[1]) {
            return true;
        }
        return false;
    }
}
