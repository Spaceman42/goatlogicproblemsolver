package org.ethan.solver.rule.ruleset.rule10;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.CarType;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule10 implements Rule {
    /*
    More people ride in the lion car than in the eagle car. No boys rode on the orange, pink or goat cars.
        There were more people riding in the first 3 cars than in the last 3 cars.
     */
    @Override
    public Evaluation evaluateSolution(Solution s) {
        Evaluation lionEagleCheck = lionEagleCheck(s);
        Evaluation boysCheck = noBoysCheck(s);
        Evaluation peopleCheck = peopleCheck(s);

        if (lionEagleCheck == Evaluation.NOT_APPLICABLE && boysCheck == Evaluation.NOT_APPLICABLE && peopleCheck == Evaluation.NOT_APPLICABLE) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (lionEagleCheck == Evaluation.FAIL || boysCheck == Evaluation.FAIL || peopleCheck == Evaluation.FAIL) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }


    private Evaluation peopleCheck(Solution s) { //More people in first 3 cars then last 3
        Car[] cars = s.getAllCars();
        if (cars.length != GivenRules.NUM_CARTS) {
            return Evaluation.NOT_APPLICABLE;
        }

        int numFirst3 = 0;
        int numLast3 = 0;
        for (int i = 0; i < 3; i++) { //first 3
            numFirst3 += cars[i].getNumPeople();
        }
        for (int i = GivenRules.NUM_CARTS - 4; i<GivenRules.NUM_CARTS; i++) { //last 3
            numLast3 += cars[i].getNumPeople();
        }

        if (numFirst3 < numLast3) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

    private Evaluation noBoysCheck(Solution s) {
        for (Car c : s.getAllCars()) {
            if (c.getType() == CarType.GOAT || c.getColor() == CarColor.ORANGE || c.getColor() == CarColor.PINK) {
                if (c.hasBoys()) {
                    return Evaluation.FAIL;
                }
            }
        }
        return Evaluation.PASS;
    }

    private Evaluation lionEagleCheck(Solution s) {
        Car lion = s.getCar(CarType.LION);
        Car eagle = s.getCar(CarType.EAGLE);

        if (lion == null || eagle == null) {
            return Evaluation.NOT_APPLICABLE;
        }

        if (lion.getNumPeople() > eagle.getNumPeople()) {
            return Evaluation.PASS;
        }

        return Evaluation.FAIL;
    }
}
