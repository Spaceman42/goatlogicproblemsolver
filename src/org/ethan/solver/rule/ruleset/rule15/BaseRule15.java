package org.ethan.solver.rule.ruleset.rule15;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.CarType;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.LastName;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule15 implements Rule {
    /*
    Bill, Monique, and Mr. Landis all rode (not necessarily in any of the same cars), ahead of Charles (who did not ride on a mammal), and Mr. Anderson.
        No girls rode on the cat, dog, or silver cars. The Eagle was somewhere behind the black car.
     */
    @Override
    public Evaluation evaluateSolution(Solution s) {
        //First check if charles is on a mammal
        Person charles = s.getPerson(FirstName.CHARLES);
        if (charles != null) {
            if (charles.getCar().getType().mammal) {
                return Evaluation.FAIL;
            }
        }


        Evaluation[] results = new Evaluation[3];
        results[0] = noGirls(s);
        results[1] = order(s);
        results[2] = eagle(s);

        for (Evaluation e : results) {
            if (e == Evaluation.FAIL) {
                return Evaluation.FAIL;
            }
        }

        return Evaluation.PASS;
    }

    private Evaluation noGirls(Solution s) {
        for (Car c : s.getAllCars()) {
            if (c.getType() == CarType.CAT || c.getType() == CarType.DOG || c.getColor() == CarColor.SILVER) {
                if (c.hasGirls()) {
                    return Evaluation.FAIL;
                }
            }
        }
        return Evaluation.PASS;
    }

    private Evaluation order(Solution s) {
        Person bill = s.getPerson(FirstName.BILL);
        Person monique = s.getPerson(FirstName.MONIQUE);
        Person landis = s.getPerson(LastName.LANDIS);
        Person charles = s.getPerson(FirstName.CHARLES);
        Person anderson = s.getPerson(LastName.ANDERSON);

        if (bill == null || monique == null || landis == null || charles == null || anderson == null) {
            return Evaluation.NOT_APPLICABLE;
        }

        int billNum = bill.getCar().getNumber();
        int moniqueNum = monique.getCar().getNumPeople();
        int landisNum = landis.getCar().getNumPeople();
        int charlesNum = charles.getCar().getNumPeople();
        int andersonNum = anderson.getCar().getNumPeople();

        if (!(billNum < charlesNum || billNum < andersonNum)) {
            return Evaluation.FAIL;
        }
        if (!(moniqueNum < charlesNum || moniqueNum < andersonNum)) {
            return Evaluation.FAIL;
        }
        if (!(landisNum < charlesNum || landisNum < andersonNum)) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

    private Evaluation eagle(Solution s) {
        Car eagle = s.getCar(CarType.EAGLE);
        if (eagle == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (eagle.getNumber() == 0) { //The eagle can't be first if it's behind the black car
            return Evaluation.FAIL;
        }
        Car black = s.getCar(CarColor.BLACK);
        if (black != null) {
            if (black.getNumber() == GivenRules.NUM_CARTS - 1) { //The black car can't be last because it is ahead of the eagle
                return Evaluation.FAIL;
            }
            if (eagle.getNumber() < black.getNumber()) {
                return Evaluation.FAIL;
            }
        }

        return Evaluation.PASS;
    }
}
