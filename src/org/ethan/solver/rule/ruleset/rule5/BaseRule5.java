package org.ethan.solver.rule.ruleset.rule5;

import org.ethan.solver.Solution;
import org.ethan.solver.data.*;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.solution.Car;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule5 implements Rule {

    /*
    From left to right: car 1--> not a lion; car 2-->
    red dragon; car 3-->horse; car 4--. 1 rider; car 5-->2 riders of the same sex;
    car 6-->orange; car 7-->Travis(boy); car
    8-->Raymond and Ms Sanchez(who did not eat popcorn); car 9-->pig; car10-->not blue.
     */


    @Override
    public Evaluation evaluateSolution(Solution s) {
        Evaluation[] results = new Evaluation[10];

        results[0] = checkIsNot(0, CarType.LION, s); //car 1--> not a lion;
        results[1] = car2(s); //2 = red dragon
        results[2] = checkIs(2, CarType.HORSE, s); //3 = horse
        results[3] = car4(s); //4 = 1 rider
        results[4] = car5(s); //5 same sex
        results[5] = checkIs(5, CarColor.ORANGE, s); //6 = orange
        results[6] = checkIs(6, FirstName.TRAVIS, s); //7 = travis
        results[7] = car8(s); //8 raymond and sanchez
        results[8] = checkIs(8, CarType.PIG, s); //9 = pig
        results[9] = checkIsNot(9, CarColor.BLUE, s); //10 = not blue

        for (Evaluation e : results) {
            if (e == Evaluation.FAIL) {
                return Evaluation.FAIL;
            }
        }

        return Evaluation.PASS;
    }

    private Evaluation car8(Solution s) {
        Evaluation raymond = checkIs(7, FirstName.RAYMOND, s);
        Evaluation sanchez = checkIs(7, LastName.SANCHEZ, s);

        if (raymond == Evaluation.NOT_APPLICABLE || sanchez == Evaluation.NOT_APPLICABLE) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (raymond == Evaluation.FAIL || sanchez == Evaluation.FAIL) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

    private Evaluation car5(Solution s) {
        Car c = s.getCar(4);
        if (c == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (c.getNumPeople() == 2) {
            if (c.getPeople()[0].getGender() == c.getPeople()[1].getGender()) {
                return Evaluation.PASS;
            }
        }
        return Evaluation.FAIL;
    }

    private Evaluation car2(Solution s) {
        Evaluation type = checkIs(1, CarType.DRAGON, s);
        Evaluation color = checkIs(1, CarColor.RED, s);

        if (type == Evaluation.NOT_APPLICABLE || color == Evaluation.NOT_APPLICABLE) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (type == Evaluation.FAIL || color == Evaluation.FAIL) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

    private Evaluation car4(Solution s) { // 1 rider
        Car c = s.getCar(3);
        if (c == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (c.getNumPeople() != 1) {
            return Evaluation.FAIL;
        }
        return Evaluation.PASS;
    }

    private Evaluation checkIsNot(int car, Data object, Solution s) {
        Car c = s.getCar(car);
        if (c == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (c.containsValue(object)) {
            return Evaluation.FAIL;
        }

        return Evaluation.PASS;
    }

    private Evaluation checkIs(int car, Data object, Solution s) {
        Car c = s.getCar(car);
        if (c == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        if (c.containsValue(object)) {
            return Evaluation.PASS;
        }
        return Evaluation.FAIL;
    }
}
