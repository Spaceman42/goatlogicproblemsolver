package org.ethan.solver.rule.ruleset.rule13;

import org.ethan.solver.Solution;
import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.CarType;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule13 implements Rule {
    //The green car was at least two places ahead of the fox car (which was not the last).
    // The yellow car was before the blue and the rooster cars. The white car was not the last.
    @Override
    public Evaluation evaluateSolution(Solution s) { //Horrible method...

        int greenPlace = -1;
        int foxPlace = -1;
        int yellowPlace = -1;
        int bluePlace = -1;
        int roosterPlace = -1;

        for (Car c : s.getAllCars()) {
            if (c.getColor() == CarColor.GREEN) {
                greenPlace = c.getNumber();
                if (greenPlace == GivenRules.NUM_CARTS - 1) { //Green can't be in last place because it is ahead of fox
                    return Evaluation.FAIL;
                }
            }
            if (c.getType() == CarType.FOX) {
                foxPlace = c.getNumber();
                if (foxPlace == GivenRules.NUM_CARTS - 1 || foxPlace == 0) {//Fox can't be in first place because it is behind green
                    return Evaluation.FAIL;
                }
            }
            if (c.getColor() == CarColor.YELLOW) {
                yellowPlace = c.getNumber();
                if (yellowPlace == GivenRules.NUM_CARTS - 1) { //Yellow car can't be in last place because it is before blue and rooster
                    return Evaluation.FAIL;
                }
            }
            if (c.getColor() == CarColor.BLUE) {
                bluePlace = c.getNumber();
                if (bluePlace == 0) { //Blue can't be in first because it is behind yellow
                    return Evaluation.FAIL;
                }
            }
            if (c.getType() == CarType.ROOSTER) {
                roosterPlace = c.getNumber();
                if (roosterPlace == 0) { //Rooser can't be in first place because it is behind yellow
                    return Evaluation.FAIL;
                }
            }
            if (c.getColor() == CarColor.WHITE) {
                if (c.getNumber() == GivenRules.NUM_CARTS - 1) { // WHite can't be last
                    return Evaluation.FAIL;
                }
            }
        }

        //The green car was at least two places ahead of the fox car (which was not the last).
        // The yellow car was before the blue and the rooster cars. The white car was not the last.
        if (greenPlace != -1 && foxPlace != -1) {
            if (foxPlace - greenPlace < 2) {
                return Evaluation.FAIL;
            }
        }
        if (yellowPlace != -1) {
            if (bluePlace != -1) {
                if (yellowPlace > bluePlace) {
                    return Evaluation.FAIL;
                }
            }
            if (roosterPlace != -1) {
                if (yellowPlace > roosterPlace) {
                    return Evaluation.FAIL;
                }
            }
        }

        return Evaluation.PASS;
    }
}
