package org.ethan.solver.rule.ruleset.rule8;

import org.ethan.solver.Solution;
import org.ethan.solver.data.Gender;
import org.ethan.solver.rule.BaseRule;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.Rule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

import java.util.Collection;

/**
 * Created by Ethan on 4/20/2015.
 */
public class BaseRule8 implements Rule {
    //All of the girls rode with a partner. Twice as many girls rode in the first half cars as in the latter half.
    @Override
    public Evaluation evaluateSolution(Solution s) {
        return partnerCheck(s);
    }


    private Evaluation partnerCheck(Solution s) { //girls have partner
        Collection<Person> people = s.getAllPeople();
        if (people == null) {
            return Evaluation.NOT_APPLICABLE;
        }
        int firstHalf = 0;
        int secondHalf = 0;

        for (Person p : people) {
            if (p != null && p.getGender() == Gender.FEMALE) {
                if (p.getCar().getNumPeople() != 2) {
                    return Evaluation.FAIL;
                }
                if (p.getCar().getNumber() < GivenRules.NUM_CARTS/2) {
                    firstHalf++;
                } else {
                    secondHalf++;
                }
            }
        }

        if (s.getAllCars().length == GivenRules.NUM_CARTS) {
            if (firstHalf*2 != secondHalf) {
                return Evaluation.FAIL;
            }
        }

        return Evaluation.PASS;
    }
}
