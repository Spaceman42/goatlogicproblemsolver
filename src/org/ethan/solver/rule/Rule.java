package org.ethan.solver.rule;

import org.ethan.solver.Solution;

/**
 * Created by Ethan on 4/20/2015.
 */
public interface Rule {
    public Evaluation evaluateSolution(Solution s);
}
