package org.ethan.solver;

import org.ethan.solver.data.*;
import org.ethan.solver.rule.Evaluation;
import org.ethan.solver.rule.PersonGenderRule;
import org.ethan.solver.rule.ruleset.GenderRule;
import org.ethan.solver.rule.ruleset.GivenRules;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

import java.util.*;

/**
 * Created by Ethan on 4/20/2015.
 */
public class CarSolver {
    private int number;

    public long iterations = 0;
    private long added = 0;

    private Collection<CarSolution> solutions;
    private Set<FirstName> people;

    private GenderRule gr;
    private GivenRules given;
    private PersonGenderRule pgr;

    public CarSolver(int number) {
        this.number = number;

        //solutions = new ArrayDeque<CarSolution>(3459776);
        people = new HashSet<FirstName>();
        gr = new GenderRule();
        given = new GivenRules();
        pgr = new PersonGenderRule();
    }


    public Collection<CarSolution> solve() {
        doWork();
        return solutions;
    }

    public Set<CarColor> getUniqueColors(Collection<CarSolution> solutions) {
        Set<CarColor> results = new HashSet<CarColor>();

        for (CarSolution s : solutions) {
            CarColor c = s.getCar().getColor();
            if (!results.contains(c)) {
                results.add(c);
            }
        }

        return results;
    }
    private void doWork() {
        for (CarType type : CarType.values()) {
            for (CarColor color : CarColor.values()) {
                noPeopleCar(type, color);
                for (FirstName fn1 : FirstName.values()) {
                    for (LastName ln1 : LastName.values()) {
                        for (Gender g1 : Gender.values()) {

                            if (pgr.evaluate(new Person(fn1, ln1, null, g1, null))) {
                                continue;
                            }

                            for (Snack s1 : Snack.values()) {

                                onePersonCar(type, color, fn1, ln1, g1, s1);
                                for (FirstName fn2 : FirstName.values()) {
                                    for (LastName ln2 : LastName.values()) {
                                        for (Gender g2 : Gender.values()) {

                                            if (pgr.evaluate(new Person(fn2, ln2, null, g2, null))) {
                                                continue;
                                            }

                                            for (Snack s2 : Snack.values()) {
                                                finalCar(type, color, fn1, ln1, g1, s1, fn2, ln2, g2, s2);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void onePersonCar(CarType type, CarColor color, FirstName fn1, LastName ln1, Gender g1, Snack s1) {
        Car car = new Car(number);
        car.setColor(color);
        car.setType(type);

        car.setPerson(0, new Person(fn1, ln1, s1, g1, car));
        checkRules(car);
    }

    private void noPeopleCar(CarType type, CarColor color) {
        Car car = new Car(number);
        car.setColor(color);
        car.setType(type);
    }

    private void finalCar(CarType t, CarColor c, FirstName f1, LastName l1, Gender g1, Snack s1, FirstName f2,
                          LastName l2, Gender g2, Snack s2) {
        Car car = new Car(number);
        car.setColor(c);
        car.setType(t);
        Person p1 = new Person(f1, l1, s1, g1, car);
        Person p2 = new Person(f2, l2, s2, g2, car);
        car.setPerson(0, p1);
        car.setPerson(1, p2);
        checkRules(car);
    }

    public void checkRules(Car c) {
        CarSolution solution = new CarSolution(c);

        //gr.evaluateSolution(solution);
        if (given.evaluateSolution(solution) != Evaluation.FAIL) {
            for (Person p : solution.getAllPeople()) {
                if (p != null) {
                    if (!people.contains(p.getFirstName())) {
                        people.add(p.getFirstName());
                    }
                }
            }
            added++;
        }
        //System.out.println("Solution built!");
        //check rules
        iterations++;
        if (iterations % 1000000 == 0) {
            double mem = Runtime.getRuntime().totalMemory();
            mem /= 1000.0;
            mem /= 1000.0;
            System.out.println("iteration " + iterations + " Memory: " + mem +" Added: " +
                    added + "ratio: " + added/(double)iterations);
        }
    }

    public long getAdded() {
        return added;
    }

    public Set<FirstName> getData() {
        return people;
    }
}
