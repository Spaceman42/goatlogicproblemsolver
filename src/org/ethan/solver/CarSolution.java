package org.ethan.solver;

import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.CarType;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.LastName;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.Person;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Ethan on 4/20/2015.
 */
public class CarSolution implements Solution{

    private Car car;


    public CarSolution(Car c) {
        this.car = c;
    }

    @Override
    public Person getPerson(FirstName name) {
        return car.getPerson(name);
    }

    @Override
    public Person getPerson(LastName name) {
        return car.getPerson(name);
    }

    @Override
    public Person[] getPeople(int carNumber) {
        if (car.getNumber() == carNumber) {
            return car.getPeople();
        }
        return null;
    }

    @Override
    public Person[] getPeople(CarType type) {
        if (car.getType() == type) {
            return car.getPeople();
        }
        return null;
    }

    @Override
    public Person[] getPeople(CarColor color) {
        if (car.getColor() == color) {
            return car.getPeople();
        }
        return null;
    }

    @Override
    public Collection<Person> getAllPeople() {
        return Arrays.asList(car.getPeople());
    }

    @Override
    public Car getCar(int number) {
        if (car.getNumber() == number) {
            return car;
        }
        return null;
    }

    @Override
    public Car getCar(CarType type) {
        if (car.getType() == type) {
            return car;
        }
        return null;
    }

    @Override
    public Car getCar(CarColor color) {
        if (car.getColor() == color) {
            return car;
        }
        return null;
    }

    @Override
    public Car getCar(FirstName name) {
        if (car.containsValue(name)) {
            return car;
        }
        return null;
    }

    @Override
    public Car getCar(LastName name) {
        if (car.containsValue(name)) {
            return car;
        }
        return null;
    }

    @Override
    public Car[] getAllCars() {
        return new Car[] {car};
    }

    @Override
    public String toString() {
        String data = "Car: " + car.getNumber() + " Type: " + car.getType() + " Color: " + car.getColor() +
                " NumPeople: " + car.getNumPeople();
        if (car.getPeople()[0] != null) {
            Person p1 = car.getPeople()[0];
            data += " P1: " + p1.getFirstName() + " " + p1.getLastName() + " " +p1.getGender() + " " + p1.getSnack();
        }
        if (car.getPeople()[1] != null) {
            Person p2 = car.getPeople()[1];
            data += " P2: " + p2.getFirstName() + " " + p2.getLastName() + " " +p2.getGender() + " " + p2.getSnack();
        }

        return data;
    }

    public Car getCar() {
        return car;
    }
}
