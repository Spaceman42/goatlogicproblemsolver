package org.ethan.solver.solution;

import org.ethan.Util;
import org.ethan.solver.data.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by 40501 on 4/20/2015.
 */
public class Car {
    private final int number;


    private Person[] people;

    private CarColor color;
    private CarType type;


    private Set<Data> fastDataSet;


    public Car(int number) {
        this.number = number;
        people = new Person[2];
        fastDataSet = new HashSet<Data>();
    }

    public CarColor getColor() {
        return color;
    }

    public void setColor(CarColor color) {
        this.color = color;
        if (this.color != null) {
            fastDataSet.remove(color);
        }
        fastDataSet.add(color);
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
        if (this.type != null) {
            fastDataSet.remove(type);
        }
        fastDataSet.add(type);
    }

    public void setPerson(int i, Person p) {
        people[i] = p;

        if (people[i] != null) {
            fastDataSet.remove((p.getFirstName()));
            fastDataSet.remove((p.getLastName()));
            fastDataSet.remove((p.getSnack()));
        }

        fastDataSet.add(p.getFirstName());
        fastDataSet.add(p.getLastName());
        fastDataSet.add((p.getSnack())); //TODO: might need to change. I think just using this for contains() is okay for now
    }

    public Person getPerson(int i) {
        return people[i];
    }

    public boolean containsValue(Data value) {
        return fastDataSet.contains(value);
    }

    public Car copy() {
        Car copy = new Car(number);
        copy.people = Util.copyOf(people);
        copy.color = color;
        copy.type = type;
        copy.fastDataSet = new HashSet<Data>(fastDataSet);

        return copy;
    }

    public int getNumber() {
        return number;
    }

    public Person[] getPeople() {
        return people;
    }

    public Person getPerson(LastName name) {
        if (containsValue(name)) {
            for (Person p : people) {
                if (p.getLastName() == name) {
                    return p;
                }
            }
        }
        return null;
    }

    public Person getPerson(FirstName name) {
        if (containsValue(name)) {
            for (Person p : people) {
                if (p.getFirstName() == name) {
                    return p;
                }
            }
        }
        return null;
    }

    public int getNumPeople() {
        int result = 0;
        if (people[0] != null) {
            result ++;
        }
        if (people[1] != null) {
            result++;
        }
        return result;
    }

    private boolean hasGender(Gender g) {
        if (people[0] != null) {
            if (people[0].getGender() == g) {
                return true;
            }
        }
        if (people[1] != null) {
            if (people[1].getGender() == g) {
                return true;
            }
        }
        return false;
    }

    public boolean hasBoys() {
        return hasGender(Gender.MALE);
    }

    public boolean hasGirls() {
        return hasGender(Gender.FEMALE);
    }
}
