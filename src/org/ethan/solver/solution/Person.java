package org.ethan.solver.solution;

import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.Gender;
import org.ethan.solver.data.LastName;
import org.ethan.solver.data.Snack;

/**
 * Created by 40501 on 4/20/2015.
 */
public class Person {

    private FirstName firstName;
    private LastName lastName;

    private Snack snack;

    private Car car;

    private Gender gender;

    public Person(FirstName firstName, LastName lastName, Snack snack, Gender gender, Car car) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.snack = snack;
        this.car = car;
        this.gender = gender;
    }

    public Person() {}

    public FirstName getFirstName() {
        return firstName;
    }

    public void setFirstName(FirstName firstName) {
        this.firstName = firstName;
    }

    public LastName getLastName() {
        return lastName;
    }

    public void setLastName(LastName lastName) {
        this.lastName = lastName;
    }

    public Snack getSnack() {
        return snack;
    }

    public void setSnack(Snack snack) {
        this.snack = snack;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
