package org.ethan.solver.solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 40501 on 4/20/2015.
 */
public class CarSolutionList {
    private List<Car> cars;


    public CarSolutionList() {
        cars = new ArrayList<Car>();
    }


    public void add(Car c) {
        cars.add(c);
    }

    public List<Car> getCars() {
        return cars;
    }
}
