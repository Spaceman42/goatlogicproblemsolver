package org.ethan.solver;

import org.ethan.solver.data.CarColor;
import org.ethan.solver.data.CarType;
import org.ethan.solver.data.FirstName;
import org.ethan.solver.data.LastName;
import org.ethan.solver.solution.Car;
import org.ethan.solver.solution.InvalidSolutionException;
import org.ethan.solver.solution.Person;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by 40501 on 4/20/2015.
 */
public class FullSolution implements Solution{

    private Map<FirstName, Person> firstNamePersonMap;
    private Map<LastName, Person> lastNamePersonMap;
    private Map<CarColor, Car> carColorCarMap;
    private Map<CarType, Car> carTypeCarMap;

    private Car[] cars;

    public FullSolution(Car[] cars) {

    }

    private void addCar(Car c) throws InvalidSolutionException {
        if (!checkValidity(c)) {
            throw new InvalidSolutionException();
        }

        firstNamePersonMap.put(c.getPerson(0).getFirstName(), c.getPerson(0));
        lastNamePersonMap.put(c.getPerson(0).getLastName(), c.getPerson(0));
        firstNamePersonMap.put(c.getPerson(1).getFirstName(), c.getPerson(1));
        lastNamePersonMap.put(c.getPerson(1).getLastName(), c.getPerson(1));

        carColorCarMap.put(c.getColor(), c);
        carTypeCarMap.put(c.getType(), c);

        cars[c.getNumber()] = c;
    }


    private boolean checkValidity(Car car) {
        if (firstNamePersonMap.containsKey(car.getPerson(0).getFirstName())) {
            return false;
        }
        if (lastNamePersonMap.containsKey(car.getPerson(0).getLastName())) {
            return false;
        }
        if (firstNamePersonMap.containsKey(car.getPerson(1).getFirstName())) {
            return false;
        }
        if (lastNamePersonMap.containsKey(car.getPerson(1).getLastName())) {
            return false;
        }
        if (carTypeCarMap.containsKey(car.getType())) {
            return false;
        }
        if (carColorCarMap.containsKey(car.getColor())) {
            return false;
        }
        return true;
    }

    @Override
    public Person getPerson(FirstName name) {
        return firstNamePersonMap.get(name);
    }

    @Override
    public Person getPerson(LastName name) {
        return lastNamePersonMap.get(name);
    }

    @Override
    public Person[] getPeople(int carNumber) {
        return getCar(carNumber).getPeople();
    }

    @Override
    public Person[] getPeople(CarType type) {
        return getCar(type).getPeople();
    }

    @Override
    public Person[] getPeople(CarColor color) {
        return getCar(color).getPeople();
    }

    @Override
    public Collection<Person> getAllPeople() {
        return lastNamePersonMap.values();
    }

    @Override
    public Car getCar(int number) {
        return cars[number];
    }

    @Override
    public Car getCar(CarType type) {
        return carTypeCarMap.get(type);
    }

    @Override
    public Car getCar(CarColor color) {
        return carColorCarMap.get(color);
    }

    @Override
    public Car getCar(FirstName name) {
        return getPerson(name).getCar();
    }

    @Override
    public Car getCar(LastName name) {
        return getPerson(name).getCar();
    }

    @Override
    public Car[] getAllCars() {
        return cars;
    }
}
