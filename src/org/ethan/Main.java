package org.ethan;

import org.ethan.solver.CarSolution;
import org.ethan.solver.CarSolver;
import org.ethan.solver.data.Data;

import java.util.Collection;

/**
 * Created by 40501 on 4/20/2015.
 */
public class Main {

    public static void main(String[] args) {
        CarSolver solverTest = new CarSolver(0);
        long time = System.currentTimeMillis();
        Collection<CarSolution> solutions = solverTest.solve();
        long dt = System.currentTimeMillis() - time;

        System.out.println(dt/1000.0f +"seconds to complete " + solverTest.iterations +" iterations");
        System.out.println("Passed " + solverTest.getAdded() + " solutions");

        for (Data c : solverTest.getData()) {
            System.out.println(c);
        }
    }
}
