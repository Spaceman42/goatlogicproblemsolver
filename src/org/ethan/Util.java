package org.ethan;

import org.ethan.solver.solution.Person;

/**
 * Created by 40501 on 4/20/2015.
 */
public class Util {

    public static <T> T[]  copyOf(T[] values, T[] output) {
        if (output.length != values.length) {
            throw new IllegalArgumentException("Output and input arrays different sizes. Cannot copy.");
        }

        for (int i = 0; i < values.length; i++) {
            output[i] = values[i];
        }

        return values;
    }


    public static Person[] copyOf(Person[] p) {
        return copyOf(p, new Person[p.length]);
    }
}
